describe('The Login Page', () => {

  it('sets auth cookie when logging in via form submission', function () {

    cy.visit('/login')

    cy.get('input[type=email]').type("johndoe@example.com")

    cy.get('input[type=password]').type("secret")

    cy.get('form').submit()

    cy.get('h1').should('contain', 'Organizations')
  })
})