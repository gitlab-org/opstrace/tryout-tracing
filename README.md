# GitLabCRM on Rails

A demo application built with Ruby on Rails to help demonstrate Distributed tracing.

![Screenshot](screenshot.png)

## Getting Started using Distributed Tracing

Clone the repo locally:

```
git clone https://gitlab.com/gitlab-org/opstrace/tryout-tracing.git
```

### Feature Flags

The following feature flags must be enabled to utilize tracing on gitlab.com in its current state:

Project level feature flag where traces are to be stored/viewed (enables tracing for this project):

```bash
/chatops run feature set --project=<your_project> observability_tracing true
```

Group level feature flag for the immediate parent of the Project specified above (enables observability scopes on auth tokens):

```bash
/chatops run feature set --group=<your_group> observability_group_tab true
```

### Auth Token

Next, at the group level for your project, navigate to Settings > Access Tokens. Create a new access token
with the following scopes:

* read_api
* read_observability
* write_observability

### Create a .env file

Copy the included `.env.example` file to a `.env` and adjust the following values using the namespace-id, project-id, and access token that you generated previously:

```
OTEL_EXPORTER=otlphttp
OTEL_EXPORTER_OTLP_TRACES_ENDPOINT=https://observe.gitlab.com/v3/<namespace-id>/<project-id>/ingest/traces
OTEL_EXPORTER_OTLP_TRACES_HEADERS="private-token=<gitlab-private-access-token>"
```

### OpenTelemetry Instrumentation

NOTE: The Opentelementry exporter is included and configured via several key files. This will autoinstrument traces to be exported in otel format.

The `Gemfile`:

```
gem "opentelemetry-sdk", "~> 1.3"
gem "opentelemetry-instrumentation-all", "~> 0.40.0"
gem "opentelemetry-exporter-otlp", "~> 0.26.1"
```

The `config/initializers/opentelemetry.rb` file:

```
require 'opentelemetry/sdk'
require 'opentelemetry/instrumentation/all'
require 'opentelemetry-exporter-otlp'

OpenTelemetry::SDK.configure do |c|
  c.service_name = 'gitlab-crm'
  c.use_all() # enables all instrumentation!
end
```

## Setting up the CRM application

Setup (install dependencies, create and seed database):

```
cd tryout-tracing
bin/setup
```

Start it:

```
bin/dev
```

You're ready to go! Visit GitlabCRM in your browser (http://localhost:8080), and login with:

- **Username:** johndoe@example.com
- **Password:** secret

Click around the mock CRM application to generate and send traces to your project.

Navigate to your GitLab project and view the traces under Monitor > Tracing

### Requirements

- Ruby 3.2
- Ruby on Rails 7
- PostgreSQL

Now you can navigate to [http://localhost:8080](http://localhost:8080) and click some buttons to generate and send traces to your project.

## Credits

- Original work by Jonathan Reinink (@reinink) and contributors
- Port to Ruby on Rails by Georg Ledermann (@ledermann)
- Forked from https://github.com/ledermann/pingcrm
